Send a message with wordle link to Telegram.

1. Create .env file:

```
TELEGRAM_TOKEN=
TELEGRAM_CHAT_ID=
```

2. Run:

```
docker build --tag wordle-sender .
```

3. Run:

```
docker run -it --env-file .env --rm wordle-sender app.py word
```

If you want to run with a cron job, use:

```
docker run --env-file .env --rm wordle-sender app.py word
```

4. If you want to run the script without Docker:

```
pip install -r requirements.txt
python app.py word
```
