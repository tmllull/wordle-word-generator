from dotenv import dotenv_values
import sys
import telegram
import os

# https://mywordle.strivemath.com/

# Check if env variables are setted
try:
    TELEGRAM_TOKEN = os.environ["TELEGRAM_TOKEN"]
    TELEGRAM_CHAT_ID = os.environ["TELEGRAM_CHAT_ID"]
except:
    #Load .env
    config = dotenv_values(".env")
    TELEGRAM_TOKEN = config["TELEGRAM_TOKEN"]
    TELEGRAM_CHAT_ID = config["TELEGRAM_CHAT_ID"]


def hash_word(text):

    dict = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8, 'i': 9, 'j': 10,
            'k': 11, 'l': 12, 'm': 13, 'n': 14, 'o': 15, 'p': 16, 'q': 17, 'r': 18, 's': 19,
            't': 20, 'u': 21, 'v': 22, 'w': 23, 'x': 24, 'y': 25, 'z': 26}
    dict2 = {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g', 8: 'h', 9: 'i', 10: 'j', 11: 'k', 12: 'l', 13: 'm',
             14: 'n', 15: 'o', 16: 'p', 17: 'q', 18: 'r', 19: 's', 20: 't', 21: 'u', 22: 'v', 23: 'w', 24: 'x', 25: 'y', 26: 'z'}
    new_text = text.lower()
    pos = 0
    result = ""
    for i in new_text:
        if pos < 6:
            pos += 1
        else:
            pos = 1
        if pos % 6 == 0:
            letter_pos = dict[i]
            if letter_pos+4 <= 26:
                new_pos = 4
                result = result + dict2[dict[i]+new_pos]
            else:
                new_pos = letter_pos+4 - 26
                result = result + dict2[new_pos]
        elif pos % 5 == 0:
            letter_pos = dict[i]
            if letter_pos+11 <= 26:
                new_pos = 11
                result = result + dict2[dict[i]+new_pos]
            else:
                new_pos = letter_pos+11 - 26
                result = result + dict2[new_pos]
        elif pos % 4 == 0:
            letter_pos = dict[i]
            if letter_pos+3 <= 26:
                new_pos = 3
                result = result + dict2[dict[i]+new_pos]
            else:
                new_pos = letter_pos+3 - 26
                result = result + dict2[new_pos]
        elif pos % 3 == 0:
            letter_pos = dict[i]
            if letter_pos+17 <= 26:
                new_pos = 17
                result = result + dict2[dict[i]+new_pos]
            else:
                new_pos = letter_pos+17 - 26
                result = result + dict2[new_pos]
        elif pos % 2 == 0:
            letter_pos = dict[i]
            if letter_pos+14 <= 26:
                new_pos = 14
                result = result + dict2[dict[i]+new_pos]
            else:
                new_pos = letter_pos+14 - 26
                result = result + dict2[new_pos]
        elif pos % 1 == 0:
            letter_pos = dict[i]
            if letter_pos+22 <= 26:
                new_pos = 22
                result = result + dict2[dict[i]+new_pos]
            else:
                new_pos = letter_pos+22 - 26
                result = result + dict2[new_pos]
    print(result)
    return result


def tg_notify(word, chat_id, token):
    msg = "Aquí tienes el wordle de Marrones. Suerte."+"\n"
    msg += "https://mywordle.strivemath.com/?word="+word+"&lang=any"
    bot = telegram.Bot(token=token)
    bot.sendMessage(chat_id=chat_id, text=msg,
                    parse_mode=telegram.ParseMode.MARKDOWN)


tg_notify(hash_word(sys.argv[1]), TELEGRAM_CHAT_ID, TELEGRAM_TOKEN)
